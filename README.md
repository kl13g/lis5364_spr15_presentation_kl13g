# References
----------
### Palmer, J. W. (2002). Web site usability, design, and performance metrics. Information systems research, 13(2), 151-167.
---
### Pernice, K., & Nielsen, J. (2008). Web usability for senior citizens: design guidelines based on usability studies with people age 65 and older. Nielsen Norman Group.
---
